﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
namespace BaixaComissao.classes
{
    class clsImportarBaixaComissao
    {
        public string LimpaTabelaBaixaComissao()
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Presenca"].ConnectionString);
                con.Open();
                SqlCommand Com = new SqlCommand("truncate table tblBaixaComissao",con);

                int Ret = Com.ExecuteNonQuery();
                return string.Concat("Tabela limpa");
            }
            catch(Exception Ex)
            { return Ex.Message; }
        }

        public string ImportaBaixaComissao(string Arquivo)
        {

            DataTable dt = new DataTable();
            DataRow row;

            try
            {

                using (System.IO.StreamReader SR = new System.IO.StreamReader(Arquivo))
                {
                    string[] colunas = SR.ReadLine().Split(';');

                    // cria colunas
                    dt.Columns.Add(new DataColumn("NumeroContrato", typeof(string)));
                    dt.Columns.Add(new DataColumn("CPF", typeof(string)));
                    dt.Columns.Add(new DataColumn("Produto", typeof(string)));
                    dt.Columns.Add(new DataColumn("Banco", typeof(string)));
                    dt.Columns.Add(new DataColumn("ValorComissao", typeof(double)));
                    dt.Columns.Add(new DataColumn("DataComissao", typeof(DateTime)));
                    dt.Columns.Add(new DataColumn("DataPagamento", typeof(DateTime)));

                    // alimenta datatable
                    while (!SR.EndOfStream)
                    {
                        string Linha = SR.ReadLine();
                        string[] LinhaTratada;
                        if (Linha.Replace(';', ' ').Trim() != string.Empty)
                        {
                            row = dt.NewRow();
                            LinhaTratada = Linha.Split(';');

                            LinhaTratada[1] = LinhaTratada[1].Replace(".", "").Replace("-", "");
                            LinhaTratada[4] = LinhaTratada[4].Replace(",", "").Replace(".", ",");

                            row.ItemArray = LinhaTratada;
                            dt.Rows.Add(row);
                        }

                    }

                }

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Presenca"].ConnectionString);
                SqlBulkCopy bc = new SqlBulkCopy(con.ConnectionString, SqlBulkCopyOptions.TableLock);

                bc.DestinationTableName = "tblBaixaComissao";
                bc.BatchSize = dt.Rows.Count;
                con.Open();
                bc.WriteToServer(dt);
                bc.Close();
                con.Close();

                return string.Concat("Importados ", dt.Rows.Count, " registros");
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ValidaImportacao(ref DataTable dt)
        {
            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Presenca"].ConnectionString);
                SqlCommand cm = new SqlCommand("Proc_BaixaComissao_Lote_Critica", con);

                dt.Columns.Add(new DataColumn("ContratoNumero", typeof(string)));
                dt.Columns.Add(new DataColumn("CPF", typeof(string)));
                dt.Columns.Add(new DataColumn("Produto", typeof(string)));
                dt.Columns.Add(new DataColumn("Banco", typeof(string)));
                dt.Columns.Add(new DataColumn("ValorComissao", typeof(double)));
                dt.Columns.Add(new DataColumn("Critica", typeof(string)));

                con.Open();

                dt.Load(cm.ExecuteReader());

                string ret = string.Empty;

                if (dt.Rows.Count == 0)
                {
                    ret = "Validação executada com sucesso, não foram geradas críticas.";
                }

                return ret;
            }
            catch (Exception Ex)
            {
                return Ex.Message;
            }

        }

        public string BaixarComissao(ref DataTable dt)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Presenca"].ConnectionString);
                SqlCommand cm = new SqlCommand("Proc_BaixaComissao_Lote", con);

                dt.Columns.Add(new DataColumn("PagId", typeof(long)));
                dt.Columns.Add(new DataColumn("pagData", typeof(DateTime)));
                dt.Columns.Add(new DataColumn("PagValor", typeof(double)));
                dt.Columns.Add(new DataColumn("UsuNome", typeof(string)));
                dt.Columns.Add(new DataColumn("FinNome", typeof(string)));
                dt.Columns.Add(new DataColumn("AgtNome", typeof(string)));
                dt.Columns.Add(new DataColumn("PagDataInclusao", typeof(DateTime)));
                dt.Columns.Add(new DataColumn("PagObservacao", typeof(string)));

                con.Open();

                dt.Load(cm.ExecuteReader());

                string ret = string.Empty;

                if (dt.Rows.Count == 0)
                {
                    ret = "Baixa de Comissão executada com sucesso, não foram gerados pagamentos.";
                }

                return ret;


                return string.Empty;
            }
            catch (Exception Ex)
            {
                return Ex.Message;
            }
            
        }

    }
}
