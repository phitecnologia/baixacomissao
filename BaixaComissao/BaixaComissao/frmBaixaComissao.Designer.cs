﻿namespace BaixaComissao
{
    partial class frmBaixaComissao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBaixaComissao));
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.txtArquivo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnImportar = new System.Windows.Forms.Button();
            this.btnValidar = new System.Windows.Forms.Button();
            this.btnBaixar = new System.Windows.Forms.Button();
            this.gridCriticas = new System.Windows.Forms.DataGridView();
            this.chkAceitarCriticas = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridCriticas)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFile.Image")));
            this.btnOpenFile.Location = new System.Drawing.Point(440, 29);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(30, 23);
            this.btnOpenFile.TabIndex = 3;
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtArquivo
            // 
            this.txtArquivo.Location = new System.Drawing.Point(23, 32);
            this.txtArquivo.Name = "txtArquivo";
            this.txtArquivo.Size = new System.Drawing.Size(411, 20);
            this.txtArquivo.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Arquivo a ser importado:";
            // 
            // btnImportar
            // 
            this.btnImportar.Location = new System.Drawing.Point(491, 32);
            this.btnImportar.Name = "btnImportar";
            this.btnImportar.Size = new System.Drawing.Size(101, 23);
            this.btnImportar.TabIndex = 6;
            this.btnImportar.Text = "Importar";
            this.btnImportar.UseVisualStyleBackColor = true;
            this.btnImportar.Click += new System.EventHandler(this.btnImportar_Click);
            // 
            // btnValidar
            // 
            this.btnValidar.Location = new System.Drawing.Point(623, 32);
            this.btnValidar.Name = "btnValidar";
            this.btnValidar.Size = new System.Drawing.Size(101, 23);
            this.btnValidar.TabIndex = 7;
            this.btnValidar.Text = "Validar";
            this.btnValidar.UseVisualStyleBackColor = true;
            this.btnValidar.Click += new System.EventHandler(this.btnValidar_Click);
            // 
            // btnBaixar
            // 
            this.btnBaixar.Enabled = false;
            this.btnBaixar.Location = new System.Drawing.Point(750, 32);
            this.btnBaixar.Name = "btnBaixar";
            this.btnBaixar.Size = new System.Drawing.Size(101, 23);
            this.btnBaixar.TabIndex = 8;
            this.btnBaixar.Text = "Baixar Comissão";
            this.btnBaixar.UseVisualStyleBackColor = true;
            this.btnBaixar.Click += new System.EventHandler(this.btnBaixar_Click);
            // 
            // gridCriticas
            // 
            this.gridCriticas.AllowUserToAddRows = false;
            this.gridCriticas.AllowUserToDeleteRows = false;
            this.gridCriticas.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.gridCriticas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCriticas.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gridCriticas.Location = new System.Drawing.Point(12, 70);
            this.gridCriticas.Name = "gridCriticas";
            this.gridCriticas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridCriticas.Size = new System.Drawing.Size(933, 254);
            this.gridCriticas.TabIndex = 9;
            this.gridCriticas.Visible = false;
            // 
            // chkAceitarCriticas
            // 
            this.chkAceitarCriticas.AutoSize = true;
            this.chkAceitarCriticas.Location = new System.Drawing.Point(750, 12);
            this.chkAceitarCriticas.Name = "chkAceitarCriticas";
            this.chkAceitarCriticas.Size = new System.Drawing.Size(210, 17);
            this.chkAceitarCriticas.TabIndex = 10;
            this.chkAceitarCriticas.Text = "Aceitar Críticas e Importar Pagamentos";
            this.chkAceitarCriticas.UseVisualStyleBackColor = true;
            this.chkAceitarCriticas.Visible = false;
            this.chkAceitarCriticas.CheckedChanged += new System.EventHandler(this.chkAceitarCriticas_CheckedChanged);
            // 
            // frmBaixaComissao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 334);
            this.Controls.Add(this.chkAceitarCriticas);
            this.Controls.Add(this.gridCriticas);
            this.Controls.Add(this.btnBaixar);
            this.Controls.Add(this.btnValidar);
            this.Controls.Add(this.btnImportar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtArquivo);
            this.Controls.Add(this.btnOpenFile);
            this.Name = "frmBaixaComissao";
            this.Text = "Baixa Comissão";
            ((System.ComponentModel.ISupportInitialize)(this.gridCriticas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.TextBox txtArquivo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnImportar;
        private System.Windows.Forms.Button btnValidar;
        private System.Windows.Forms.Button btnBaixar;
        private System.Windows.Forms.DataGridView gridCriticas;
        private System.Windows.Forms.CheckBox chkAceitarCriticas;
    }
}

