﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaixaComissao
{
    public partial class frmBaixaComissao : Form
    {
        public frmBaixaComissao()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            openFileDialog.Filter = "Arquivos CSV|*.csv";
            openFileDialog.Title = "Selecione o arquivo para importação";

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                txtArquivo.Text = openFileDialog.FileName;
            }
      
        }

        private void btnImportar_Click(object sender, EventArgs e)
        {

            gridCriticas.Visible = false;
            chkAceitarCriticas.Visible = false;
            btnBaixar.Enabled = false;

            string Ret = Validacoes(txtArquivo.Text) ;

            if (Ret.Trim() != string.Empty)
            {
                MessageBox.Show(Ret);
                return;
            }

            classes.clsImportarBaixaComissao Imp = new classes.clsImportarBaixaComissao();

            string MsgCompleta = Imp.LimpaTabelaBaixaComissao(); 

            Ret = Imp.ImportaBaixaComissao(txtArquivo.Text);

            if (Ret.Trim() == string.Empty)
            {
                Ret = "Arquivo Importado com Sucesso!";
            }

            MsgCompleta = string.Concat(MsgCompleta, System.Environment.NewLine , Ret);

            MessageBox.Show(MsgCompleta);

        }

        private string Validacoes(string Arquivo)
        {

            if (txtArquivo.Text.Trim() == string.Empty)
            {
                return "Informe o arquivo a ser importado.";
            }

            if (System.IO.File.Exists(txtArquivo.Text) == false)
            {
                return "Arquivo não encontrato.";
            }

            return string.Empty;
        }

        private void btnValidar_Click(object sender, EventArgs e)
        {

            classes.clsImportarBaixaComissao Imp = new classes.clsImportarBaixaComissao();
            DataTable dt = new DataTable(); 
            string Ret = Imp.ValidaImportacao(ref dt);

            gridCriticas.DataSource = dt;

            gridCriticas.Visible = dt.Rows.Count > 0;
            chkAceitarCriticas.Visible = gridCriticas.Visible;
            btnBaixar.Enabled = !chkAceitarCriticas.Visible; 

            if (Ret != string.Empty)
            {
                MessageBox.Show(Ret);
            }

        }

        private void btnBaixar_Click(object sender, EventArgs e)
        {

            classes.clsImportarBaixaComissao Imp = new classes.clsImportarBaixaComissao();
            DataTable dt = new DataTable();
            string Ret = Imp.BaixarComissao(ref dt);

            gridCriticas.DataSource = dt;

            gridCriticas.Visible = dt.Rows.Count > 0;

            if (dt.Rows.Count == 0)
            {
                Ret = "Não foram gerados pagamentos. Verifique se há importação realizada.";
                chkAceitarCriticas.Visible = false;
                chkAceitarCriticas.Checked = false;
                btnBaixar.Enabled = false;
            }

            if (Ret != string.Empty)
            {
                MessageBox.Show(Ret);
            }
            else
            {
                btnBaixar.Enabled = false;
                chkAceitarCriticas.Checked = false;
                chkAceitarCriticas.Visible = false;
            }
        }

        private void chkAceitarCriticas_CheckedChanged(object sender, EventArgs e)
        {

            btnBaixar.Enabled = chkAceitarCriticas.Checked;

        }
    }
}
