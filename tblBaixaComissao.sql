if not exists(select 1 from sysobjects where name = 'tblBaixaComissao')
begin
	Create table tblBaixaComissao
	(
		ContratoNumero	varchar(50)	,
		CPF				varchar(15)	,
		Produto			varchar(50)	,
		Banco			varchar(50)	,
		ValorComissao	money		,
		DataComissao	datetime	,
		DataPagamento	datetime
	)
end


