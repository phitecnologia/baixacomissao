use analise
if exists(select 1 from sysobjects where name = 'Proc_BaixaComissao_Lote_Critica')
Begin
	Drop procedure Proc_BaixaComissao_Lote_Critica
End

GO

Create procedure Proc_BaixaComissao_Lote_Critica

As

Create Table #tmpCritica
(
	ContratoNumero	varchar(50),
	CPF				varchar(15),
	Produto			varchar(50),
	Banco			varchar(50),
	ValorComissao	money,
	Critica			varchar(100)	
)

--contrato n�o cadastrado
Insert Into #tmpCritica
(
	ContratoNumero, CPF, Produto, Banco, ValorComissao, Critica
)
select	ContratoNumero, CPF, Produto, Banco, ValorComissao, 'Contrato n�o encontrado'
from	tblBaixaComissao	tmp
where	not exists	(	select	'x'
						from	consigonline.dbo.tblcliente	cli (nolock) 
								inner join consigonline.dbo.tblbeneficio	ben (nolock) on
								(	cli.cliid			= ben.cliid
								--and	ben.BenStatus		= 1
								)
								inner join consigonline.dbo.tblcontrato	ctr (nolock) on
								(	ben.benid			= ctr.benid 
								)
								inner join ConsigOnline.dbo.tblFinanceira	fin (nolock) on
								(	ctr.finid			= fin.finid
								--and	tmp.banco			= fin.finnome collate Latin1_General_CI_AS
								)
						where	tmp.CPF	= cli.CliCPFCNPJ collate Latin1_General_CI_AS
						and	ltrim( rTrim( tmp.CONTRATONUMERO ) )	= ltrim( rTrim( ctr.CtrNumeroContrato ) ) collate Latin1_General_CI_AS
					)
and		valorcomissao	 > 0

--DROP TABLE Safra



-- contrato sem comiss�o gerada
Insert Into #tmpCritica
(
	ContratoNumero, CPF, Produto, Banco, ValorComissao, Critica
)
select	ContratoNumero, CPF, Produto, Banco, ValorComissao,'Contrato sem comiss�o gerada'
FROM	tblBaixaComissao	tmp
		inner join consigonline.dbo.tblcliente	cli (nolock) on
		(	tmp.CPF				= cli.clicpfcnpj	collate Latin1_General_CI_AS
		)
		inner join consigonline.dbo.tblbeneficio	ben (nolock) on
		(	cli.cliid			= ben.cliid
		)
		inner join consigonline.dbo.tblcontrato	ctr (nolock) on
		(	ben.benid			= ctr.benid 
		and	ltrim( rTrim( tmp.CONTRATONUMERO ) )	= ltrim( rTrim( ctr.CtrNumeroContrato ) ) collate Latin1_General_CI_AS
		)
		inner join ConsigOnline.dbo.tblFinanceira	fin (nolock) on
		(	ctr.finid			= fin.finid
		--and	tmp.banco			= fin.finnome collate Latin1_General_CI_AS
		)
where	valorcomissao 	> 0
and		not exists	(	select	'x'
						from	consigonline.dbo.tblcontratooperacao						cop (nolock)
								inner join consigonline.dbo.tblcontratooperacaocomissao	coc (nolock) on
								(	cop.copid							= coc.copid
								and	coc.cocprevisto						= 0
								and	coc.usuid							is null
								)
								inner join consigonline.dbo.tblcontratooperacaocomissaolancamento	ccl (nolock) on
								(	coc.cocid							= ccl.cocid
								)
						where	cop.ctrid	= ctr.ctrid
					)
and		valorcomissao	> 0

-- valor de comiss�o zerada
Insert Into #tmpCritica
(
	ContratoNumero, CPF, Produto, Banco, ValorComissao, Critica
)
select	ContratoNumero, CPF, Produto, Banco, ValorComissao,'Comiss�o com valor zerado'
from	tblBaixaComissao tmp
Where	valorcomissao	= 0
and		not exists	(	select	'x'
						from	tblBaixaComissao tmp2
						where	tmp.CPF	= tmp2.CPF
						and		tmp.ContratoNumero = tmp2.ContratoNumero	collate Latin1_General_CI_AS
						and		valorComissao	> 0
					)

declare @linhas as integer = 0

select @linhas = count(*) from tblbaixaComissao

if @linhas = 0
begin
	insert into #tmpCritica (Critica) values ('Tabela de baixa de comiss�o em branco, importe um arquivo para valid�-lo')
end


Select 	NumeroContrato	,
		CPF				,
		Produto			,
		Banco			,
		ValorComissao	,
		Critica			
From #TmpCritica
Order by cpf, ContratoNumero, Banco, ValorComissao