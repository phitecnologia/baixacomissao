use analise
go

if exists(select 1 from sysobjects where name = 'Proc_BaixaComissao_Lote')
Begin
	drop Procedure dbo.Proc_BaixaComissao_Lote
End
GO

Create Proc dbo.Proc_BaixaComissao_Lote

as

	Set NoCount On

	Declare	@PagDataInclusao	DateTime

	Select	@PagDataInclusao	= GetDate()

	-- INSERE O LOTE DE PAGAMENTO
	 Insert Into ConsigOnline.dbo.tblPagamento
	 (	PagData,
		PagValor,
		PagObservacao,
		FinId,
		PagDataInclusao
	)
	SELECT	tmp.dataPagamento,
			--sum( round( cast( replace( replace( [valor da comiss�o], '.', '' ), ',', '.' ) as money ), 2, 1  ) ) ,
			sum( round( cast( tmp.valorcomissao as money ), 2, 1  ) ) ,
			'Pagamento gerado via lote de importa��o, comiss�es de ' + convert( varchar(10), min( tmp.DATACOMISSAO ),  103 ) + ' � ' + convert( varchar(10), max( tmp.DATACOMISSAO ),  103 ), 
			Fin.FinId,
			@PagDataInclusao
	FROM	tblBaixaComissao	tmp
			inner join consigonline.dbo.tblcliente	cli (nolock) on
			(	tmp.CPF				= cli.clicpfcnpj	collate Latin1_General_CI_AS
			)
			inner join consigonline.dbo.tblbeneficio	ben (nolock) on
			(	cli.cliid			= ben.cliid
			)
			inner join consigonline.dbo.tblcontrato	ctr (nolock) on
			(	ben.benid			= ctr.benid 
			and	ltrim( rTrim( tmp.CONTRATONUMERO ) )	= ltrim( rTrim( ctr.CtrNumeroContrato ) ) collate Latin1_General_CI_AS
			)
			inner join ConsigOnline.dbo.tblFinanceira	fin (nolock) on
			(	ctr.finid			= fin.finid
			--and	tmp.banco			= fin.finnome collate Latin1_General_CI_AS
			)
	where	cast( tmp.valorcomissao as money )	> 0
	and		exists	(	select	'x'
						from	consigonline.dbo.tblcontratooperacao						cop (nolock)
								inner join consigonline.dbo.tblcontratooperacaocomissao	coc (nolock) on
								(	cop.copid							= coc.copid
								and	coc.cocprevisto						= 0
								and	coc.usuid							is null
								)
								inner join consigonline.dbo.tblcontratooperacaocomissaolancamento	ccl (nolock) on
								(	coc.cocid							= ccl.cocid
								)
						where	cop.ctrid	= ctr.ctrid
					)
	GROUP BY
		tmp.datapagamento,
		Fin.FinId

	-- INSERE A COMPOSI��O DO PAGAMENTO
	Insert Into Consigonline.dbo.tblPagamentoComissao
	(	PcoValor,
		PcoObservacao,
		CclId,
		PagId
	)
	SELECT	--round( cast( [valor da comiss�o] as money ) * ( Ccl.CclValor / Ccl_Total.CclValor_Total ), 2, 1 ),
			--round( cast( replace( replace( [valor da comiss�o], '.', '' ), ',', '.' ) as money ), 2, 1  ) ,
			round(	cast( tmp.valorcomissao as money ) * 
				(	Cast( Ccl.CclValor as Decimal( 18, 8 ) ) 
				/	Cast( Ccl_Total.CclValor_Total as Decimal( 18, 8 ) ) 
				), 2, 1 ),
			'Pagamento baixado via importa��o de arquivo',
			Ccl.CclId,
			Pag.PagId
	FROM	tblBaixaComissao	tmp
			inner join consigonline.dbo.tblcliente	cli (nolock) on
			(	tmp.CPF				= cli.clicpfcnpj	collate Latin1_General_CI_AS
			)
			inner join consigonline.dbo.tblbeneficio	ben (nolock) on
			(	cli.cliid			= ben.cliid
			)
			inner join consigonline.dbo.tblcontrato	ctr (nolock) on
			(	ben.benid			= ctr.benid 
			and	ltrim( rTrim( tmp.CONTRATONumero ) )	= ltrim( rTrim( ctr.CtrNumeroContrato ) ) collate Latin1_General_CI_AS
			)
			inner join ConsigOnline.dbo.tblFinanceira	fin (nolock) on
			(	ctr.finid			= fin.finid
			--and	tmp.banco			= fin.finnome collate Latin1_General_CI_AS
			)
			inner join consigonline.dbo.tblcontratooperacao						cop (nolock) on
			(	cop.ctrid	= ctr.ctrid
			)
			inner join consigonline.dbo.tblcontratooperacaocomissao	coc (nolock) on
			(	cop.copid							= coc.copid
			and	coc.cocprevisto						= 0
			and	coc.usuid							is null
			)
			inner join consigonline.dbo.tblcontratooperacaocomissaolancamento	ccl (nolock) on
			(	coc.cocid							= ccl.cocid
			)
			Inner Join	(	Select	CopId,
									Sum( CclValor )	as CclValor_Total
							From	(	Select	Distinct 
												Cop.CopId,
												Ccl.CclId,
												Ccl.CclValor
										From	tblBaixaComissao	tmp
												inner join consigonline.dbo.tblcliente	cli (nolock) on
												(	tmp.CPF				= cli.clicpfcnpj	collate Latin1_General_CI_AS
												)
												inner join consigonline.dbo.tblbeneficio	ben (nolock) on
												(	cli.cliid			= ben.cliid
												)
												inner join consigonline.dbo.tblcontrato	ctr (nolock) on
												(	ben.benid			= ctr.benid 
												and	ltrim( rTrim( tmp.CONTRATONUMERO ) )	= ltrim( rTrim( ctr.CtrNumeroContrato ) ) collate Latin1_General_CI_AS
												)
												inner join ConsigOnline.dbo.tblFinanceira	fin (nolock) on
												(	ctr.finid			= fin.finid
												--and	tmp.banco			= fin.finnome collate Latin1_General_CI_AS
												)
												inner join consigonline.dbo.tblcontratooperacao						cop (nolock) on
												(	cop.ctrid	= ctr.ctrid
												)
												inner join consigonline.dbo.tblcontratooperacaocomissao	coc (nolock) on
												(	cop.copid							= coc.copid
												and	coc.cocprevisto						= 0
												and	coc.usuid							is null
												)
												inner join consigonline.dbo.tblcontratooperacaocomissaolancamento	ccl (nolock) on
												(	coc.cocid							= ccl.cocid
												)
										Where	cast( tmp.valorcomissao as money )	> 0
									)	Cop
							Group By
									CopId
						)	Ccl_Total on
			(	Cop.CopId	= Ccl_Total.CopId
			)
			inner join consigonline.dbo.tblPagamento	Pag (NoLock) on
			(	Pag.PagData							= tmp.datapagamento
			and	Pag.FinId							= Fin.FinId
			and	Pag.PagDataInclusao					= @PagDataInclusao
			)
	where	cast( tmp.valorcomissao as money )	> 0
	
	Declare	@CopId			BigInt,
			@PagId			BigInt,
			@ValorDiferenca	Money

	Declare	Cursor_Ajuste
	Insensitive Cursor For
	Select	Cop.CopId,
			Pag.PagId,
			[valor da comiss�o] - Sum( Pco.PcoValor )
	From	consigonline.dbo.tblcliente	cli (nolock) 
			inner join consigonline.dbo.tblbeneficio	ben (nolock) on
			(	cli.cliid			= ben.cliid
			)
			inner join consigonline.dbo.tblcontrato	ctr (nolock) on
			(	ben.benid			= ctr.benid 			
			)
			inner join ConsigOnline.dbo.tblFinanceira	fin (nolock) on
			(	ctr.finid			= fin.finid
			)
			inner join consigonline.dbo.tblcontratooperacao						cop (nolock) on
			(	cop.ctrid	= ctr.ctrid
			)
			inner join consigonline.dbo.tblcontratooperacaocomissao	coc (nolock) on
			(	cop.copid							= coc.copid
			and	coc.cocprevisto						= 0
			and	coc.usuid							is null
			)
			inner join consigonline.dbo.tblcontratooperacaocomissaolancamento	ccl (nolock) on
			(	coc.cocid							= ccl.cocid
			)
			inner join consigonline.dbo.tblPagamento	Pag (NoLock) on
			(	Pag.FinId							= Fin.FinId
			and	Pag.PagDataInclusao					= @PagDataInclusao
			)
			Inner Join ConsigOnline.dbo.tblPagamentoComissao	Pco (NoLock) on
			(	Pag.PagId							= Pco.PagId
			and	Ccl.CclId							= Pco.CclId
			)
			inner join	(	Select	tmp.CPF,
									tmp.datapagamento,
									tmp.CONTRATONUMERO,
									round( Sum( cast( tmp.valorcomissao as money ) ), 2, 1 )	as [valor da comiss�o]
							From	tblBaixaComissao	tmp
							Group By
									tmp.CPF,
									tmp.datapagamento,
									tmp.CONTRATONUMERO
						)	Tmp on
			(	tmp.CPF				= cli.clicpfcnpj	collate Latin1_General_CI_AS
			and	Pag.PagData			= tmp.datapagamento
			and	ltrim( rTrim( tmp.CONTRATONUMERO ) )	= ltrim( rTrim( ctr.CtrNumeroContrato ) ) collate Latin1_General_CI_AS
			)
			
	Group By
			Cop.CopId,
			Pag.PagId,
			[valor da comiss�o]
	Having
			[valor da comiss�o]	<> Sum( Pco.PcoValor )


	Open	Cursor_Ajuste

	Fetch Next From Cursor_Ajuste
	Into	@CopId,
			@PagId,
			@ValorDiferenca

	While	@@Fetch_Status	= 0
	Begin

		Declare	@CclId	BigInt,
				@PcoId	BigInt

		Select	@CclId	= null
		
		Select	Top 1
				@CclId	= CclId
		From	consigonline.dbo.tblcontratooperacao					cop (nolock)
				inner join consigonline.dbo.tblcontratooperacaocomissao	coc (nolock) on
				(	cop.copid							= coc.copid
				and	coc.cocprevisto						= 0
				and	coc.usuid							is null
				)
				inner join consigonline.dbo.tblcontratooperacaocomissaolancamento	ccl (nolock) on
				(	coc.cocid							= ccl.cocid
				)
		Where	Cop.CopId	= @CopId
		Order By
				Ccl.CclValor	Desc


		If	@CclId	Is Not Null
		Begin

			Select	Top 1 
					@PcoId	= PcoId
			From	ConsigOnline.dbo.tblPagamentoComissao
			Where	PagId		= @PagId
			and		CclId		= @CclId
			Order By PcoValor Desc

			Update	ConsigOnline.dbo.tblPagamentoComissao
			Set		PcoValor	= PcoValor + @ValorDiferenca
			Where	PcoId		= @PcoId
		End
		

		Fetch Next From Cursor_Ajuste
		Into	@CopId,
				@PagId,
				@ValorDiferenca

	End

	Close	Cursor_Ajuste
	DealLocate	Cursor_Ajuste

	truncate table tblBaixaComissao

	Select	Pag.PagId           ,
			Pag.PagData         ,  
			Pag.PagValor        ,
			Pag.PagObservacao   ,                                                                                                                                                                                                                                          
			Usu.UsuNome			,
			Fin.FinNome			,
			Agt.AgtNome			,
			Pag.PagDataInclusao
	From	ConsigOnline.dbo.tblPagamento pag
			Left join ConsigOnline.dbo.tblFinanceira fin (nolock) on pag.finid = fin.finid
			Left join ConsigOnline.dbo.tblusuario    usu (nolock) on pag.usuid = usu.usuid
			Left join ConsigOnline.dbo.tblagente     agt (nolock) on pag.AgtId = agt.AgtId
	Where	PagDataInclusao	= @PagDataInclusao
